<?php

/**
 * 配置文件
 */

return [
    // 开启调试模式
    'debug' => true,
    // 是否追踪
    'trace' => true,
    // 是否显示错误信息
    'display_error' => true,

    // 设置时区
    'timezone' => 'Asia/Shanghai',
    // 语言
    'locale' => 'zh-cn',

    // 默认模块
    'default_module'     => 'home',
    // 默认控制器
    'default_controller' => 'Home',
    // 默认方法
    'default_method'     => 'index',

    // 开启路由
    'route' => false,
    // 不允许注册
    'not_allowed_join' => false,

    // 文件后缀
    'suffix' => '.html',

    // 版本
    'version' => '0.0.1',
    // 是否安装
    'installed' => 1,

    // 数据库
    'db' => [
        'driver'   => 'mysql',
        'host'     => '127.0.0.1',
        'database' => 'mini_test',
        // 'username' => 'mini_test',
        // 'password' => 'HspSR5DLdemhekdw',
        'username' => 'root',
        'password' => 'root',
        'charset'  => 'utf8mb4',
        'port'     => 3306,
        'prefix'   => '',
    ],

    // session
    'session' => [
        // 是否全应用启动 session
        'start'  => true,
        // session 存储类型  [file, memcache, redis]
        'driver' => 'file',
        // 文件型 sessions 文件存放路径
        'path'   => './runtime/sessions/',
        //session 类似为 memcache 或 redis 时，对应的主机地址 [memcache 11211 redis 6379]
        'host'   => 'tcp://127.0.0.1:11211',
    ],

    // 缓存类型
    'allowCacheType' => [
        'file',
        'memcache',
        'redis',
    ],

    // 设置缓存
    'cache' => [
        'start'    => true,
        'driver'   => 'file',
        'path'     => './runtime/cache',
        // 主机地址 [ 'memcache', 'redis' 需要设置 ]
        'host'     => '127.0.0.1',
        // 对应各类服务的密码, 为空代表不需要密码
        'password' => '',
        // 对应服务的端口
        'port'     => '6379',
        'prefix'   => 'cache_'
    ],
];
