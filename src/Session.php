<?php

namespace mini;

class Session
{
    private static $save_path = false;

    public static function start(): void
    {
        if (self::$save_path === false) {
            session_save_path(ROOT_PATH . 'runtime/sessions');
            self::$save_path = true;
        }

        (session_status() === PHP_SESSION_NONE) ? session_start([
            'use_strict_mode' => true,
        ]) : null;
    }

    /**
     * 如果会话变量存在，返回 true，否则返回 false
     *
     * @param string $key
     * @return boolean
     */
    public static function has(string $key): bool
    {
        return array_key_exists($key, $_SESSION) ? true : false;
    }

    /**
     * 获取会话值
     *
     * @param string $key
     *
     * @return mixed
     */
    public static function get($key, $default = null)
    {
        return (self::has($key)) ? $_SESSION[$key] : $default;
    }

    /**
     * 设置会话值
     *
     * @param  array|string  $param
     * @param  string $value
     *
     * @return void
     */
    public static function set($param, $value = false)
    {
        if (is_array($param)) {
            foreach ($param as $key => $v) {
                $_SESSION[$key] = $v;
            }
        } else {
            $_SESSION[$param] = $value;
        }
    }

    /**
     * 根据键删除会话数据
     *
     * @param  string $key
     * @return boolean
     */
    public static function delete($key): bool
    {
        if (self::has($key)) {
            unset($_SESSION[$key]);
        }

        return false;
    }

    /**
     * 删除全部会话数据
     *
     * @return void
     */
    public static function destroy(): void
    {
        $_SESSION = [];
        session_unset();
        session_destroy();
    }

    /**
     * 以秒数指定发送到浏览器的 cookie 的生命周期
     *
     * @param  string $sec
     *
     * @return void
     */
    public static function lifetime($sec = '0'): void
    {
        ini_set('session.cookie_lifetime', $sec);
        ini_set('session.gc_maxlifetime', $sec);
    }

    /**
     * 生成 csrf token
     */
    // public static function generateToken(int $length = 32)
    // {
    //     Session::start();
    //     if (! Session::has('csrf_token')) {
    //         self::set('csrf_token', bin2hex(random_bytes($length)));
    //     }
    // }

    /**
     * 获取 csrf token
     */
    public static function getToken(int $length = 32)
    {
        if (empty($_SESSION['csrf_token'])) {
            self::set('csrf_token', bin2hex(random_bytes($length)));
        }

        $token = self::get('csrf_token');
        self::delete('csrf_token');

        return $token;
    }
}
