<?php

/*
 * 文件型缓存支持类
*/

namespace mini\caches;

class FileCacher
{

    private static $cacher = null;
    private $cacheDir      = 'caches';

    private function __construct($config)
    {
        $this->cacheDir = CACHE_DATA_FILES . DS;
        if (!is_dir($this->cacheDir)) {
            mkdir($this->cacheDir, 0777, true);
        }
    }

    public static function getInstance($config)
    {
        if (self::$cacher == null) {
            self::$cacher = new FileCacher($config);
        }
        return self::$cacher;
    }

    public function get($name)
    {
        $cacheFile = $this->cacheDir . $name . '.php';
        if (!is_file($cacheFile)) {
            return false;
        }
        $cacheData = require $cacheFile;
        $cacheData = unserialize($cacheData);
        if ($cacheData['expire'] < time()) {
            return false;
        }
        return $cacheData['data'];
    }

    public function set($name, $data, $expire)
    {
        $cacheFile = $this->cacheDir . $name . '.php';
        $cacheContent = '<?php
if(!defined("SYSTEM_PATH")){exit();}
$data = <<<EOF
';
        $cacheData = array(
            'data'   => $data,
            'expire' => time() + $expire
        );
        $cacheData = str_replace('\\', '\\\\', serialize($cacheData));
        $cacheData = str_replace('$', '\$', $cacheData);
        $cacheContent .=  $cacheData . '
EOF;
return $data;';
        file_put_contents($cacheFile, $cacheContent);
    }

    public function removeCache($name)
    {
        $cacheFile = $this->cacheDir . $name . '.php';
        if (!is_file($cacheFile)) {
            return true;
        }
        unlink($cacheFile);
        return true;
    }

    public function clearCache()
    {
        $files = scandir($this->cacheDir);
        foreach ($files as $v) {
            if ($v != '.' && $v != '..') {
                $cacheUrl = $this->cacheDir . $v;
                if (is_file($cacheUrl)) {
                    @unlink($cacheUrl);
                }
            }
        }
        return true;
    }

    public function close()
    {
    }
}
