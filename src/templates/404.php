<!doctype html>
<html lang="zh-cn">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>404 Not Found</title>
    <style>
        *,*::before,*::after{box-sizing:border-box}
        body{margin:0;font-family:-apple-system,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans","Liberation Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";font-size:1rem;line-height:1.5;color:#000;text-align:left;background-color:#f6f6f6}
        .exception{margin:150px auto 0 auto;padding:2rem 2rem 1rem 2rem;width:600px;background-color:#fff;border-top:5px solid #dc3545;word-break:break-word;box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15)}
        a{color: #1a73e8;text-decoration: none;}
        .exception h1{padding:0;margin:0 0 4px 0;font-size:1.5rem;font-weight:normal;color:#666}
        .text{margin-bottom:1.5rem;font-size:1.5rem;line-height:1.25;font-weight:600;color:#332F51}
        .list{padding:1.5rem 0 0 0;border-top:1px solid #ddd;line-height:2}
        .list dt{float:left;margin-right:1rem;color:#666}
        .fs-48{font-size:3rem !important}
        .fs-20{font-size:1.25rem !important}
    </style>
</head>

<body>
    <div class="exception">
        <h1 class="fs-48">404</h1>
        <p><?= $msg; ?></p>
        <p>
            <a href="/" class="fs-20">&#8592 返回</a>
        </p>
    </div>
</body>

</html>