<?php

namespace mini;

class Templates
{
    public function public($data) {
        $this->load('public', $data);
    }

    public function error_404($data) {
        $this->load('error_404', $data);
    }

    private function load($template_file, $data = null) {
        $file_path = ROOT_PATH . 'templates/' . $template_file . '.php';

        if (file_exists($file_path)) {
            if (isset($data)) {
                extract($data);
            }

            require_once($file_path);
        } else {
            throw new \Exception('模板不存在: '.$file_path);
        }
    }
}